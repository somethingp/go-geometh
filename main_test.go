// Package geometh provides functions for downloading methylation data from the NCBI
// GEO database, and parsing it into structs that are easily usable in GoLang.
package geometh

import (
	"encoding/json"
	"io/ioutil"
	"reflect"
	"testing"
)

func TestGet(t *testing.T) {
	type args struct {
		GEOCode               string
		betaValueColumnHeader string
	}

	var geoData GEOSoftData

	geoData.Load("testfiles/GSE30653_family.soft.golden")

	tests := []struct {
		name    string
		args    args
		want    GEOSoftData
		wantErr bool
	}{
		{"Nazor27K", args{"GSE30653", ""}, geoData, false},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Get(tt.args.GEOCode, tt.args.betaValueColumnHeader)
			if (err != nil) != tt.wantErr {
				t.Errorf("Get() error = %v, wantErr %v\n", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(*got, tt.want) {
				t.Errorf("Get() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFilterByPlatformInfo(t *testing.T) {
	type args struct {
		geoData          GEOSoftData
		informationTitle string
		information      string
	}

	var geoData GEOSoftData
	err := geoData.Load("testfiles/GSE30653_family.soft.golden")
	checkErr(err)

	var ChrXData GEOSoftData
	err = ChrXData.Load("testfiles/ChrXJson.soft.golden")
	checkErr(err)

	tests := []struct {
		name             string
		args             args
		wantFilteredData GEOSoftData
	}{
		{"Geo", args{geoData, "Chr", "X"}, ChrXData},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotFilteredData := FilterByPlatformInfo(tt.args.geoData, tt.args.informationTitle, tt.args.information); !reflect.DeepEqual(gotFilteredData, tt.wantFilteredData) {
				t.Errorf("FilterByPlatformInfo() = %v, want %v", gotFilteredData, tt.wantFilteredData)
			}
		})
	}
}

func TestCalculateMean(t *testing.T) {
	type args struct {
		geoData GEOSoftData
	}

	XMethJSON, err := ioutil.ReadFile("testfiles/XMeth.golden.json")
	checkErr(err)
	var XMeth map[string]float64
	err = json.Unmarshal(XMethJSON, &XMeth)
	checkErr(err)

	var ChrXData GEOSoftData
	err = ChrXData.Load("testfiles/ChrXJson.soft.golden")
	checkErr(err)

	tests := []struct {
		name                string
		args                args
		wantMeanMethylation map[string]float64
	}{
		{"ChrX", args{ChrXData}, XMeth},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotMeanMethylation := CalculateMean(tt.args.geoData); !reflect.DeepEqual(gotMeanMethylation, tt.wantMeanMethylation) {
				t.Errorf("CalculateMean() = %v, want %v", gotMeanMethylation, tt.wantMeanMethylation)
			}
		})
	}
}
