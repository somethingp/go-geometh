package geometh

import (
	"compress/gzip"
	"encoding/gob"
	"fmt"
	"log"
	"os"
)

func checkErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func checkErrDesc(err error, description string) {
	if err != nil {
		fmt.Println(description)
		log.Fatal(err)
	}
}

func (g *GEOSoftData) append(g2 GEOSoftData) GEOSoftData {
	combinedPlatform := g.Platform
	combinedSamples := g.Samples

	for name, platformRow := range g2.Platform {
		combinedPlatform[name] = platformRow
	}

	for name, sample := range g2.Samples {
		combinedSamples[name] = sample
	}

	return GEOSoftData{combinedPlatform, combinedSamples}
}

func (g *GEOSoftData) Save(filename string) error {
	fi, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer fi.Close()

	fz := gzip.NewWriter(fi)
	defer fz.Close()

	encoder := gob.NewEncoder(fz)
	err = encoder.Encode(&g)
	if err != nil {
		return err
	}

	return nil

}

func (g *GEOSoftData) Load(filename string) error {
	fi, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer fi.Close()

	fz, err := gzip.NewReader(fi)
	if err != nil {
		return err
	}
	defer fz.Close()

	decoder := gob.NewDecoder(fz)
	err = decoder.Decode(g)
	if err != nil {
		return err
	}

	return nil

}
