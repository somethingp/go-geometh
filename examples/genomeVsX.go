package main

import (
	"fmt"
	"image/color"
	"strings"

	"bitbucket.org/somethingp/go-geometh"
	"gonum.org/v1/gonum/stat"
	"gonum.org/v1/plot"
	"gonum.org/v1/plot/plotter"
	"gonum.org/v1/plot/vg"
)

func main() {
	GEOCode := "GSE30653"

	geoData, err := geometh.Get(GEOCode, "")
	if err != nil {
		panic(err)
	}

	plotGenomeMethylationVsXMethylation(geoData)

}

func plotGenomeMethylationVsXMethylation(geoData geometh.GEOSoftData) {

	// We only want to look at stem cells, so we're filtered on that.
	stemCellSamples := filterUndifferentiatedStemCells(geoData)

	// Since we're looking at X chromosome mehtylation here, we'll filter by the gender (we only want to keep the females).
	femaleStemCellSamples := filterByGender(stemCellSamples, "Female")

	// now we calculate the mean genome methylation  for each sample (after our filtering)
	// by using the function that geometh provides.
	meanGenomeMethylation := geometh.CalculateMean(femaleStemCellSamples)

	// We want to do the same thing for only the X chromosome data. First we have to
	// filter the X chromosome data, and then we calculate the mean as above.
	XChromosomeData := filterByChromosome(femaleStemCellSamples, "X")
	meanXMethylation := geometh.CalculateMean(XChromosomeData)

	// I've included a helper function in this example that takes methylation data, and
	// makes it easy to plot using the gonum plot package. The format of the image will
	// be whatever you put in the extension. Gonum/plot supports many types including
	// tiff, svg, png, and jpg.
	makePlot(meanGenomeMethylation,
		meanXMethylation,
		"Mean Genome Methylation vs. Mean X Chromosome Methylation",
		"Genome Methylation",
		"X Chromosome Methylation",
		"genomeMethylationVsXMethylation.png")

}

// filterUndifferentiatedStemCells only keeps the samples that have the characteristic of cell type being
// either  "embryonic stem cell, undifferentiated" or "induced pluripotent stem cell, undifferentiated".
// If you want to do something similar to this, you have to look at the data you're analyzing and see where
// the information you want to filter on is being stored. If you want to filter on the platform information,
// geometh provides a helper function (as used by filterByChromosome() in this example). This function only
// keeps the samples that match the required cell type.
func filterUndifferentiatedStemCells(geoData geometh.GEOSoftData) (filteredData geometh.GEOSoftData) {

	filteredData.Platform = geoData.Platform
	filteredData.Samples = make(map[string]geometh.Sample)

	for name, sample := range geoData.Samples {
		if sample.Channels[0].Characteristics["cell type"] == "embryonic stem cell, undifferentiated" ||
			sample.Channels[0].Characteristics["cell type"] == "induced pluripotent stem cell, undifferentiated" {
			filteredData.Samples[name] = sample
		}
	}

	return
}

// filterByGender is similar to filterUndifferentiatedStemCells, except that it filters by the gender of the sample.
func filterByGender(geoData geometh.GEOSoftData, gender string) (filteredData geometh.GEOSoftData) {
	filteredData.Platform = geoData.Platform
	filteredData.Samples = make(map[string]geometh.Sample)
	for name, sample := range geoData.Samples {
		if strings.ToLower(sample.Channels[0].Characteristics["gender"]) == strings.ToLower(gender) {
			filteredData.Samples[name] = sample
		}
	}

	return
}

// filterByChromosome is slightly different from filterByGender and filterUndifferentiatedStemCells
// in that it is filtering based on platform information, not information for each sample. In effect what this does
// is only keep the sample methylation data that matches the platform conditions you're giving it.
func filterByChromosome(geoData geometh.GEOSoftData, chromosome string) geometh.GEOSoftData {
	return geometh.FilterByPlatformInfo(geoData, "Chr", chromosome)
}

// makePlot is a utility function that takes the methylation data and plots it using the gonum plot
// package. More information about the plotting package can be found at https://github.com/gonum/plot
func makePlot(xAxisMethylation, yAxisMethylation map[string]float64, title, xLabel, yLabel, saveName string) {
	// Create a new plot, set its title and
	// axis labels.
	p, err := plot.New()
	if err != nil {
		panic(err)
	}

	p.Title.Text = title
	p.X.Label.Text = xLabel
	p.Y.Label.Text = yLabel
	// Draw a grid behind the data
	p.Add(plotter.NewGrid())

	//Create plot data
	pts := plotter.XYs{}
	for id, meanX := range xAxisMethylation {
		meanY := yAxisMethylation[id]
		pts = append(pts, struct{ X, Y float64 }{
			X: meanX,
			Y: meanY,
		})
	}

	// Make a scatter plotter and set its style.
	s, err := plotter.NewScatter(pts)
	if err != nil {
		panic(err)
	}
	s.GlyphStyle.Color = color.RGBA{R: 255, B: 128, A: 255}

	//Calculate the linear regression for the data
	var xs, ys []float64
	for _, pt := range pts {
		xs = append(xs, pt.X)
		ys = append(ys, pt.Y)
	}
	alpha, beta := stat.LinearRegression(xs, ys, nil, false)
	r2 := stat.RSquared(xs, ys, nil, alpha, beta)
	//Graph the function alpha + beta*x
	regression := plotter.NewFunction(func(x float64) float64 { return alpha + (beta * x) })
	regression.Color = color.RGBA{B: 0, A: 255, R: 0, G: 0}
	regression.Width = vg.Points(1)

	// Add the plotters to the plot, with a legend
	// entry for each
	p.Add(s, regression)
	p.Legend.Add("scatter", s)
	p.Legend.Add(fmt.Sprintf("y=%.2f+%.2f*x\nR2=%.2f", alpha, beta, r2), regression)

	// Save the plot to a file with the given name and extension in saveName.
	if err := p.Save(10*vg.Inch, 10*vg.Inch, saveName); err != nil {
		panic(err)
	}
}
