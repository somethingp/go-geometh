package geometh

// GEOSoftData holds the platform information and samples together for ease of use.
type GEOSoftData struct {
	Platform map[string]PlatformRow `json:"platform"`
	Samples  map[string]Sample      `json:"samples"`
}

// PlatformRow holds the platform information for each site. PlatformRow.ID corresponds
// to IDRef in Methylation data. Information is a map that actually holds all the details,
// and can be used for filtering methylation sites during your analysis. The Information field
// maps the header name to the value for that methylation site.
type PlatformRow struct {
	ID          string            `json:"id,omitempty"`
	Name        string            `json:"name,omitempty"`
	Information map[string]string `json:"information,omitempty"`
}

// Sample is the methylation sample. Along with the methylation data, it also stores the meta data for each sample.
type Sample struct {
	ID                string              `json:"id,omitempty"`
	Name              string              `json:"name,omitempty"`
	Title             string              `json:"title,omitempty"`
	GeoAccession      string              `json:"geo_accession,omitempty"`
	Status            string              `json:"status,omitempty"`
	SubmissionDate    string              `json:"submission_date,omitempty"`
	LastUpdateDate    string              `json:"last_update_date,omitempty"`
	Type              string              `json:"type,omitempty"`
	Channels          []SampleChannelInfo `json:"channels,omitempty"`
	HybProtocol       string              `json:"hyb_protocol,omitempty"`
	ScanProtocol      string              `json:"scan_protocol,omitempty"`
	Description       string              `json:"description,omitempty"`
	DataProcessing    string              `json:"data_processing,omitempty"`
	PlatformID        string              `json:"platform_id,omitempty"`
	ContactName       string              `json:"contact_name,omitempty"`
	ContactEmail      string              `json:"contact_email,omitempty"`
	ContactLaboratory string              `json:"contact_laboratory,omitempty"`
	ContactDepartment string              `json:"contact_department,omitempty"`
	ContactInstitute  string              `json:"contact_institute,omitempty"`
	ContactAddress    string              `json:"contact_address,omitempty"`
	ContactCity       string              `json:"contact_city,omitempty"`
	ContactState      string              `json:"contact_state,omitempty"`
	ContactZip        string              `json:"contact_zip,omitempty"`
	ContactCountry    string              `json:"contact_country,omitempty"`
	SupplementaryFile string              `json:"supplementary_file,omitempty"`
	SeriesID          []string            `json:"series_id,omitempty"`
	Data              []MethylationData   `json:"data,omitempty"`
}

// SampleChannelInfo stores the channel information (since there can be multiple channels)
type SampleChannelInfo struct {
	SourceName      string            `json:"source_name,omitempty"`
	Organism        string            `json:"organism,omitempty"`
	Taxid           string            `json:"taxid,omitempty"`
	Characteristics map[string]string `json:"characteristics,omitempty"`
	Molecule        string            `json:"molecule,omitempty"`
	ExtractProtocol string            `json:"extract_protocol,omitempty"`
	Label           string            `json:"label,omitempty"`
	LabelProtocol   string            `json:"label_protocol,omitempty"`
}

// MethylationData is the actual methylation data for each sample. To get
// more information about the methylation site, you have to use the Platform
// information stored in the GEOSoftData you're looking at.
type MethylationData struct {
	IDRef     string  `json:"id_ref,omitempty"`
	BetaValue float64 `json:"beta_value,omitempty"`
}
