// Package geometh provides functions for downloading methylation data from the NCBI
// GEO database, and parsing it into structs that are easily usable in GoLang.
package geometh

import (
	"bufio"
	"compress/gzip"
	"errors"
	"fmt"
	"strconv"
	"strings"

	"github.com/jlaffaye/ftp"
)

// Get downloads and parses methylation data from NCBI GEO given a GEOCode,
// and the column name that contains the beta value in the SOFT file (betaValueColumnHeader).
// If betaValueColumnHeader is empty(""), it will default to either looking for a column
// with a description containing the string "beta". If no such column is found, it'll use
// the column with the header "VALUE". If neither of these is found, it'll stop parsing and throw the
// error "no beta column defined, and no column could be automatically identified".
// It returns the methylation data, and any encountered errors.
func Get(GEOCode, betaValueColumnHeader string) (*GEOSoftData, error) {
	softGEOReader := DownloadSoft(GEOCode)
	defer softGEOReader.Close()

	softGEOScanner := bufio.NewScanner(softGEOReader)

	currentSection := ""
	currentSampleID := ""
	betaColumnHeader := ""
	betaColumnNumber := -1
	var currentSample Sample
	var currentSampleChannel []SampleChannelInfo

	manifest := make(map[string]PlatformRow)
	headers := make(map[string]int)
	samples := make(map[string]Sample)
	sampleCount := 0
	sampleRowIndex := 0

	for softGEOScanner.Scan() {
		line := softGEOScanner.Text()

		if line == "!platform_table_begin" {
			currentSection = "platform_table"
		} else if line == "!platform_table_end" {
			currentSection = ""
		} else if strings.HasPrefix(line, "^SAMPLE = ") {
			currentSection = "sample_metadata"
			currentSampleID = line[10:]
			currentSample = Sample{
				ID: line[10:],
			}
		} else if line == "!sample_table_begin" {
			currentSection = "sample_table"
			sampleRowIndex = 0
			continue
		} else if line == "!sample_table_end" {
			currentSection = ""
			betaColumnHeader = ""
			betaColumnNumber = -1
			currentSample.Channels = currentSampleChannel
			samples[currentSampleID] = currentSample
		} else if strings.HasPrefix(line, "!Series_sample_id") {
			sampleCount++
		} else if strings.HasPrefix(line, "!Platform_data_row_count") {
			platformCount, err := strconv.Atoi(strings.Split(line, " = ")[1])
			if err != nil {
				checkErrDesc(err, "error parsing platform count: '"+line+"'")
			}

			manifest = make(map[string]PlatformRow, platformCount)
		}

		if sampleCount > 0 && !strings.HasPrefix(line, "!Series_sample_id") {
			samples = make(map[string]Sample, sampleCount)
			sampleCount = 0
		}

		switch currentSection {
		case "platform_table":
			splitLine := strings.Split(line, "\t")
			if strings.HasPrefix(line, "ID") {
				//in header row
				for i, header := range splitLine {
					headers[header] = i
				}
			} else {
				manifest[splitLine[headers["ID"]]] = PlatformRow{
					ID:          splitLine[headers["ID"]],
					Name:        splitLine[headers["Name"]],
					Information: make(map[string]string),
				}

				for header, i := range headers {
					manifest[splitLine[headers["ID"]]].Information[strings.ToLower(header)] = splitLine[i]
				}

			}
		case "sample_metadata":
			if strings.HasPrefix(line, "!Sample_title") {
				currentSample.Title = strings.TrimPrefix(line, "!Sample_title = ")
			} else if strings.HasPrefix(line, "!Sample_geo_accession") {
				currentSample.GeoAccession = strings.TrimPrefix(line, "!Sample_geo_accession = ")
			} else if strings.HasPrefix(line, "!Sample_status") {
				currentSample.Status = strings.TrimPrefix(line, "!Sample_status = ")
			} else if strings.HasPrefix(line, "!Sample_submission_date") {
				currentSample.SubmissionDate = strings.TrimPrefix(line, "!Sample_submission_date = ")
			} else if strings.HasPrefix(line, "!Sample_last_update_date") {
				currentSample.LastUpdateDate = strings.TrimPrefix(line, "!Sample_last_update_date = ")
			} else if strings.HasPrefix(line, "!Sample_type") {
				currentSample.Type = strings.TrimPrefix(line, "!Sample_type = ")
			} else if strings.HasPrefix(line, "!Sample_hyb_protocol") {
				currentSample.HybProtocol = strings.TrimPrefix(line, "!Sample_hyb_protocol = ")
			} else if strings.HasPrefix(line, "!Sample_scan_protocol") {
				currentSample.ScanProtocol = strings.TrimPrefix(line, "!Sample_scan_protocol = ")
			} else if strings.HasPrefix(line, "!Sample_description") {
				currentSample.Description = strings.TrimPrefix(line, "!Sample_description = ")
			} else if strings.HasPrefix(line, "!Sample_data_processing") {
				currentSample.DataProcessing = strings.TrimPrefix(line, "!Sample_data_processing = ")
			} else if strings.HasPrefix(line, "!Sample_platform_id") {
				currentSample.PlatformID = strings.TrimPrefix(line, "!Sample_platform_id = ")
			} else if strings.HasPrefix(line, "!Sample_contact_name") {
				currentSample.ContactName = strings.TrimPrefix(line, "!Sample_contact_name = ")
			} else if strings.HasPrefix(line, "!Sample_contact_email") {
				currentSample.ContactEmail = strings.TrimPrefix(line, "!Sample_contact_email = ")
			} else if strings.HasPrefix(line, "!Sample_contact_laboratory") {
				currentSample.ContactLaboratory = strings.TrimPrefix(line, "!Sample_contact_laboratory = ")
			} else if strings.HasPrefix(line, "!Sample_contact_department") {
				currentSample.ContactDepartment = strings.TrimPrefix(line, "!Sample_contact_department = ")
			} else if strings.HasPrefix(line, "!Sample_contact_institute") {
				currentSample.ContactInstitute = strings.TrimPrefix(line, "!Sample_contact_institute = ")
			} else if strings.HasPrefix(line, "!Sample_contact_address") {
				currentSample.ContactAddress = strings.TrimPrefix(line, "!Sample_contact_address = ")
			} else if strings.HasPrefix(line, "!Sample_contact_city") {
				currentSample.ContactCity = strings.TrimPrefix(line, "!Sample_contact_city = ")
			} else if strings.HasPrefix(line, "!Sample_contact_state") {
				currentSample.ContactState = strings.TrimPrefix(line, "!Sample_contact_state = ")
			} else if strings.HasPrefix(line, "!Sample_contact_zip") {
				currentSample.ContactZip = strings.TrimPrefix(line, "!Sample_contact_zip = ")
			} else if strings.HasPrefix(line, "!Sample_contact_country") {
				currentSample.ContactCountry = strings.TrimPrefix(line, "!Sample_contact_country = ")
			} else if strings.HasPrefix(line, "!Sample_supplementary_file") {
				currentSample.SupplementaryFile = strings.TrimPrefix(line, "!Sample_supplementary_file = ")
			} else if strings.HasPrefix(line, "!Sample_series_id") {
				currentSample.SeriesID = append(currentSample.SeriesID, strings.TrimPrefix(line, "!Sample_series_id = "))
			} else if strings.HasPrefix(line, "!Sample_channel_count") {
				channelcount, err := strconv.Atoi(strings.TrimPrefix(line, "!Sample_channel_count = "))
				checkErrDesc(err, "error above Line 156")
				currentSampleChannel = make([]SampleChannelInfo, channelcount)
			} else if strings.HasPrefix(line, "!Sample_source_name") {
				index, err := strconv.Atoi(strings.TrimPrefix(line, "!Sample_source_name")[3:4])
				checkErrDesc(err, "error above Line 160")
				index = index - 1
				currentSampleChannel[index].SourceName = strings.TrimPrefix(line, "!Sample_source_name")[7:]
			} else if strings.HasPrefix(line, "!Sample_organism") {
				index, err := strconv.Atoi(strings.TrimPrefix(line, "!Sample_organism")[3:4])
				checkErrDesc(err, "error above Line 165")
				index = index - 1
				currentSampleChannel[index].Organism = strings.TrimPrefix(line, "!Sample_organism")[7:]
			} else if strings.HasPrefix(line, "!Sample_taxid") {
				index, err := strconv.Atoi(strings.TrimPrefix(line, "!Sample_taxid")[3:4])
				checkErrDesc(err, "error above Line 170")
				index = index - 1
				currentSampleChannel[index].Taxid = strings.TrimPrefix(line, "!Sample_taxid")[7:]
			} else if strings.HasPrefix(line, "!Sample_characteristics") {
				index, err := strconv.Atoi(strings.TrimPrefix(line, "!Sample_characteristics")[3:4])
				checkErrDesc(err, "error above Line 175")
				index = index - 1
				characteristic := strings.Split(strings.TrimPrefix(line, "!Sample_characteristics")[7:], ": ")
				if currentSampleChannel[index].Characteristics == nil {
					currentSampleChannel[index].Characteristics = make(map[string]string)
				}
				currentSampleChannel[index].Characteristics[characteristic[0]] = characteristic[1]
			} else if strings.HasPrefix(line, "!Sample_molecule") {
				index, err := strconv.Atoi(strings.TrimPrefix(line, "!Sample_molecule")[3:4])
				checkErrDesc(err, "error above Line 184")
				index = index - 1
				currentSampleChannel[index].Molecule = strings.TrimPrefix(line, "!Sample_molecule_")[7:]
			} else if strings.HasPrefix(line, "!Sample_extract_protocol") {
				index, err := strconv.Atoi(strings.TrimPrefix(line, "!Sample_extract_protocol")[3:4])
				checkErrDesc(err, "error above Line 189")
				index = index - 1
				currentSampleChannel[index].ExtractProtocol = strings.TrimPrefix(line, "!Sample_extract_protocol")[7:]
			} else if strings.HasPrefix(line, "!Sample_label_protocol") {
				index, err := strconv.Atoi(strings.TrimPrefix(line, "!Sample_label_protocol")[3:4])
				checkErrDesc(err, "error above Line 199")
				index = index - 1
				currentSampleChannel[index].LabelProtocol = strings.TrimPrefix(line, "!Sample_label_protocol")[7:]
			} else if strings.HasPrefix(line, "!Sample_label") { //this has to come after sample_label_protocol to avoid confusion
				index, err := strconv.Atoi(strings.TrimPrefix(line, "!Sample_label")[3:4])
				checkErrDesc(err, "error above Line 194")
				index = index - 1
				currentSampleChannel[index].Label = strings.TrimPrefix(line, "!Sample_label")[7:]
			} else if strings.HasPrefix(line, "#") {
				if (betaValueColumnHeader == "" && strings.Contains(strings.ToLower(line), "beta")) || (betaValueColumnHeader != "" && strings.Contains(line, betaValueColumnHeader)) {
					betaColumnHeader = line[strings.Index(line, "#")+1 : strings.Index(line, " = ")]
				}

			} else if strings.HasPrefix(line, "!Sample_data_row_count") {
				sampleRowCount, err := strconv.Atoi(strings.Split(line, " = ")[1])
				if err != nil {
					checkErrDesc(err, fmt.Sprintf("error parsing sample row count to integer: %s", string(strings.Split(line, " = ")[1])))
				}

				currentSample.Data = make([]MethylationData, sampleRowCount)

			}

		case "sample_table":
			if betaColumnNumber == -1 {
				splitLine := strings.Split(line, "\t")
				for i, header := range splitLine {
					if (betaColumnHeader != "" && header == betaColumnHeader) ||
						(betaColumnHeader == "" && header == "VALUE") {
						betaColumnNumber = i
					}
				}

				if betaColumnNumber == -1 {
					return &GEOSoftData{}, errors.New("no beta column defined, and no column could be automatically identified")
				}
			} else {
				//doing this instead of strings.Split is a lot more memory efficient

				i := strings.Index(line, "\t")
				idRef := line[:i]

				for j := 0; j < betaColumnNumber-1; j++ {
					i += 1 + strings.Index(line[i+1:], "\t")
				}
				nextIdx := i + 1 + strings.Index(line[i+1:], "\t")

				var betaStr string
				if nextIdx == i {
					betaStr = line[i+1:]
					fmt.Println(line)
				} else {
					betaStr = line[i+1 : nextIdx]
				}
				beta := 0.0

				if betaStr != "" {
					var err error
					beta, err = strconv.ParseFloat(betaStr, 64)
					checkErrDesc(err, "couldn't parse beta value")

				}

				currentSample.Data[sampleRowIndex] = MethylationData{
					IDRef:     idRef,
					BetaValue: beta,
				}
				sampleRowIndex++
			}
		}
	}

	checkErr(softGEOScanner.Err())

	fmt.Printf("Processed %d samples from GEO %s.\n", len(samples), GEOCode)

	return &GEOSoftData{
		Platform: manifest,
		Samples:  samples,
	}, nil
}

// DownloadSoft downloads the SOFT file from NCBI GEO, and returns a string containing
// the file's contents. This function doesn't need to be used unless custom file parsing is
// required by the user. If you think your case should be handled by default by geometh.Get,
// please enter a feature request.
// It takes a GEOCOde, and returns a string with the file's contents.
func DownloadSoft(GEOCode string) *gzip.Reader {
	// Connect to the server
	client, err := ftp.Dial("ftp.ncbi.nlm.nih.gov:21")
	checkErr(err)

	// Log in the server
	err = client.Login("anonymous", "anonymous")
	checkErr(err)

	// Retrieve the file
	reader, err := client.Retr(fmt.Sprintf("/geo/series/%snnn/%s/soft/%s_family.soft.gz", GEOCode[0:len(GEOCode)-3], GEOCode, GEOCode))
	checkErrDesc(err, "error with retrieval")

	gzippedFileReader, err := gzip.NewReader(reader)
	checkErrDesc(err, "error doing gzip read")
	defer gzippedFileReader.Close()

	return gzippedFileReader
}

// FilterByPlatformInfo is a convinient function for filtering parsed data based on
// Platform information. informationTitle is the column header name for the information,
// and information is value for the column you want to filter on. If keep is true, the matching
// information is returned. If keep is false, the matching information is removed.
func FilterByPlatformInfo(geoData GEOSoftData, informationTitle, information string, keep bool) (filteredData GEOSoftData) {

	filteredData.Samples = make(map[string]Sample, len(geoData.Samples))

	informationTitle = strings.ToLower(informationTitle)

	//the rows are counted first to help make the function slightly more memory efficient at the cost of a small increase to computational time.

	filteredPlatformCount := 0

	for _, manifest := range geoData.Platform {
		if strings.EqualFold(manifest.Information[informationTitle], information) {
			filteredPlatformCount++
		}
	}

	ids := make(map[string]bool, filteredPlatformCount)
	filteredData.Platform = make(map[string]PlatformRow, filteredPlatformCount)

	for name, manifest := range geoData.Platform {
		if strings.EqualFold(manifest.Information[informationTitle], information) == keep {

			ids[manifest.ID] = true
			filteredData.Platform[name] = manifest
		}
	}

	for name, sample := range geoData.Samples {
		filteredSample := sample

		filteredRowCount := 0
		for _, data := range sample.Data {
			if ids[data.IDRef] {
				filteredRowCount++
			}
		}

		filteredSample.Data = make([]MethylationData, filteredRowCount)

		i := 0

		for _, data := range sample.Data {
			if ids[data.IDRef] {
				filteredSample.Data[i] = data
				i++
			}
		}

		filteredData.Samples[name] = filteredSample
	}

	return
}

// CalculateMean is a convinient function that returns the mean beta value for each
// sample given GeoSoftData. meanMethylation maps the IDRef of the sample to the
// average methylation as a float64 value.
func CalculateMean(geoData GEOSoftData) (meanMethylation map[string]float64) {
	meanMethylation = make(map[string]float64, len(geoData.Samples))
	for id, sample := range geoData.Samples {
		totalBetas := 0.0
		for _, methData := range sample.Data {
			totalBetas += methData.BetaValue
		}
		meanBeta := totalBetas / float64(len(sample.Data))

		meanMethylation[id] = meanBeta
	}

	return
}
