package main

import (
	"encoding/json"
	"io/ioutil"
	"log"

	geometh "bitbucket.org/somethingp/go-geometh"
)

func main() {

	geoData, err := geometh.Get("GSE30653", "")
	checkErr(err)
	err = geoData.Save("GSE30653_family.soft.golden")
	checkErr(err)

	ChrXData := geometh.FilterByPlatformInfo(*geoData, "Chr", "X")
	err = ChrXData.Save("ChrXJson.soft.golden")

	XMethylation := geometh.CalculateMean(ChrXData)
	XMethJSON, _ := json.Marshal(XMethylation)
	err = ioutil.WriteFile("XMeth.golden.json", XMethJSON, 0644)
	checkErr(err)

}

func checkErr(err error /*, description string*/) {
	if err != nil {
		//fmt.Println(description)
		log.Fatal(err)
	}
}
